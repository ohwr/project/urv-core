.. uRV documentation master file, created by
   sphinx-quickstart on Mon Feb  5 01:42:25 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to uRV's documentation!
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
