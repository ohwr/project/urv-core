..  Headings guide:
    # with overline, for parts
    * with overline, for chapters
    = for sections
    - for subsections
    ^ for subsubsections
    " for paragraphs

###############
Getting Started
###############

Not much here for now.

.. code-block:: 

    git clone https://ohwr.org/project/urv-core.git
    cd urv-code
    . sourceme
    fusesoc list-cores

**********************
Running Verilator Lint
**********************

.. code-block::

    fusesoc run --target lint ohwr:urv:urv_cpu

    